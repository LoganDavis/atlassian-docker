#!/bin/bash

# From the trimmed down blog post, db creation is automated
# ---------------------------------------------------------
# We need to note down the IP address of the container so
# that we can wire it up to Stash and JIRA later:
# 
#     sudo docker inspect postgres | grep IPAddress
# 
# Note down the IP Address. To test that PostgreSQL is running you can can
# connect to it easily with (password: docker):
# 
#     psql -h <ip-address-noted> -d docker -U docker -W
# 
# (You may need to install the sql client with: `sudo apt-get install
# postgresql-client`).
# 
# We'll need to setup databases for both Stash and JIRA, if you want to do it now
# at the `psql` shell type:
# 
#     
#     CREATE ROLE stashuser WITH LOGIN PASSWORD 'jellyfish' VALID UNTIL 'infinity';
#     CREATE DATABASE stash WITH ENCODING='UTF8' OWNER=stashuser TEMPLATE=template0 CONNECTION LIMIT=-1;
# 
# The above creates `stash` database with `stashuser` user and password `jellyfish`. And for JIRA:
# 
#     CREATE ROLE jiradbuser WITH LOGIN PASSWORD 'jellyfish' VALID UNTIL 'infinity';
#     CREATE DATABASE jiradb WITH ENCODING 'UNICODE' TEMPLATE=template0;
# 
# The above creates `jiradb` database with `jiradbuser` user and password `jellyfish`.
# --------------------------------------------------------


# Creates Stash and JIRA databases and users

PSQL_PORT="5432"

#allow script to run either inside or outside of docker container
if [ ! -f "/.dockerinit" ]; then 
  CONTAINER="$(sudo docker inspect -f '{{ .Config.Hostname }}' postgres)"
  # Get varible info from container logs
  POSTGRES_PASS="$(sudo docker logs $CONTAINER|grep POSTGRES_PASS| cut -d "=" -f 2 )"
  POSTGRES_USER="$(sudo docker logs $CONTAINER|grep POSTGRES_USER| cut -d "=" -f 2 )"
  # Get IP Address of postgres container
  PSQL_IP="$(sudo docker inspect -f '{{ .NetworkSettings.IPAddress }}' postgres)"
else
  #use local variables
  PSQL_IP=$DB_PORT_5432_TCP_ADDR
  PSQL_PORT=$DB_PORT_5432_TCP_PORT
fi

# Saves password
echo "$PSQL_IP:*:*:$POSTGRES_USER:$POSTGRES_PASS" > $HOME/.pgpass
chmod 0600 $HOME/.pgpass

#run the db init
echo "
CREATE ROLE stashuser WITH LOGIN PASSWORD 'jellyfish' VALID UNTIL 'infinity';
CREATE DATABASE stash WITH ENCODING='UTF8' OWNER=stashuser TEMPLATE=template0 CONNECTION LIMIT=-1;
CREATE ROLE jiradbuser WITH LOGIN PASSWORD 'jellyfish' VALID UNTIL 'infinity';
CREATE DATABASE jiradb WITH ENCODING 'UNICODE' TEMPLATE=template0;" \
| PGPASSWORD="$POSTGRES_PASS" psql -h $PSQL_IP -p $PSQL_PORT -d template1 -U $POSTGRES_USER -w

#these settings will be necessary when configuring stash/jira
echo "POSTGRES CONNECTION SETTINGS"
echo "SERVER IP: $PSQL_IP"
echo "POSTGRES USER: $POSTGRES_USER"
echo "POSTGRES PASSWORD: $POSTGRES_PASS"

