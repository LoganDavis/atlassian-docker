SUDO=sudo

$SUDO docker run -d --name postgres -p=5432:5432 paintedfox/postgresql

$SUDO docker build -t durdn/atlassian-base base

cd "$(dirname $0)"
cat initialise_db.sh | $SUDO docker run --rm -i --link postgres:db paintedfox/postgresql bash -

#lkd - 20160216 stash docker container not starting
$SUDO docker build -t durdn/stash stash
STASH_VERSION="$($SUDO docker run --rm durdn/stash sh -c 'echo $STASH_VERSION')"
$SUDO docker tag durdn/stash durdn/stash:$STASH_VERSION -f

$SUDO docker run -d --name stash --link postgres:db -p 7990:7990 -p 7999:7999 durdn/stash

$SUDO docker build -t durdn/jira jira
JIRA_VERSION="$($SUDO docker run --rm durdn/jira sh -c 'echo $JIRA_VERSION')"
$SUDO docker tag durdn/jira durdn/jira:$JIRA_VERSION -f

#start jira, linking postgres and stash
#$SUDO docker run -d --name jira --link postgres:db --link stash:stash -p 8080:8080 durdn/jira

#start jira, linking postgres only
$SUDO docker run -d --name jira --link postgres:db -p 8080:8080 durdn/jira

echo "Containers running..."
$SUDO docker ps

echo "USE THE FOLLOWING ADDRESSES WHEN CONFIGURING STASH/atlassian-jira"
echo "IP Addresses of containers:"
$SUDO docker inspect -f '{{ .Config.Hostname }} {{ .Config.Image }} {{ .NetworkSettings.IPAddress }}' postgres stash jira



CONTAINER="$(sudo docker inspect -f '{{ .Config.Hostname }}' postgres)"
POSTGRES_PASS="$(sudo docker logs $CONTAINER|grep POSTGRES_PASS| cut -d "=" -f 2 )"
POSTGRES_USER="$(sudo docker logs $CONTAINER|grep POSTGRES_USER| cut -d "=" -f 2 )"
  PSQL_IP="$(sudo docker inspect -f '{{ .NetworkSettings.IPAddress }}' postgres)"

echo
echo "========================================"
echo "POSTGRES CONNECTION SETTINGS"
echo "SERVER IP: $PSQL_IP"
echo "POSTGRES USER: $POSTGRES_USER"
echo "POSTGRES PASSWORD: $POSTGRES_PASS"
echo "========================================"


